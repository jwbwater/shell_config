# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# enable history auto-completion
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

# command aliases
alias h='history'
alias ll='ls -al'
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls -al --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# increase history length
HISTSIZE=1000
HISTFILESIZE=2000

# customize command prompt
force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

# set default editor
EDITOR=nvim

# if ssh-agent is running, update env vars
SOCKETS=$(find /tmp/ -type s -name agent.\* 2> /dev/null | grep '/tmp/ssh-.*/agent.*')
if [ -z "$SOCKETS" ]; then
    # no ssh-agent running
    eval $(ssh-agent)
else
    export SSH_AUTH_SOCK=$(expr match "$SOCKETS" '.*\(/tmp/ssh-.*/agent.*\)')
    export SSH_AGENT_PID=$(($(echo "$SSH_AUTH_SOCK" | cut -d. -f2) + 1))
fi

# add snap's bin directory to path, for doctl (digital ocean)
export PATH="/var/lib/snapd/snap/bin:$PATH"

# add composer bin directory to path
export PATH="~/.config/composer/vendor/bin:$PATH"

# add npm bin directory to path
export PATH="~/.npm-global/bin:$PATH"

# add .local/bin to path
export PATH="~/.local/bin:$PATH"

# seems to be unnecessary
# add .cargo/bin to path for rust
# export PATH="~/.cargo/bin:$PATH"

# for digital ocean's doctl command line completion
# doctl completion bash > /etc/bash_completion.d/doctl
if [ -f /etc/profile.d/bash_completion.sh ]; then
    source /etc/profile.d/bash_completion.sh
fi
