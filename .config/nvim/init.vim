set nocompatible
set encoding=utf8
syntax on

" enable :find to search all subdirectories
set path+=**

" use the silver searcher for searching accross files
set grepprg=ag

" get file explorer to open files in a split on the right
let g:netrw_altv=1

" Seed YCM with keywords
"let g:ycm_seed_identifiers_with_syntax = 1
" Disable preview of definitions
"let g:ycm_add_preview_to_completeopt = 0

" Manually load YCM, to keep Vim startup fast
" Note that typing :YouCompleteMe to trigger the on clause above results in a
" mildly annoying error message that YouCompleteMe is not an editor command
"command YCM call plug#load('YouCompleteMe')

" Ignore .pyc and vim swap files in NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$']

"""""""""""""""""""""""""""""""""""""
" KEYBOARD SHORTCUTS
"""""""""""""""""""""""""""""""""""""
" Disable arrow movement, resize splits instead.
let g:elite_mode=1
if get(g:, 'elite_mode')
    nnoremap <Up>    :resize +2<CR>
    nnoremap <Down>  :resize -2<CR>
    nnoremap <Left>  :vertical resize -2<CR>
    nnoremap <Right> :vertical resize +2<CR>
endif

map <C-n> :NERDTreeToggle<CR>
map <C-m> :TagbarToggle<CR>

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"""""""""""""""""""""""""""""""""""""
" SETTINGS
"""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax on
autocmd BufEnter * :syntax sync fromstart

" required for filetype specific plugins (e.g. Python code folding)
filetype plugin indent on

" Set transparency for MacVim GUI
if has("gui_macvim")
    " set macvim specific stuff
    set transparency=15
endif

" Set font and font size
set guifont=Menlo\ Regular:h16

" Enable easy switching between file buffers without saving
set hidden

" Better command-line completion
set wildmenu

" Show partial commands in the last line of the screen
set showcmd

" Highlight searches
set hlsearch

" Enable case insensitive searches except when using capital letters
set ignorecase
set smartcase

set nostartofline

" Always show status line
set laststatus=2

" Show cursor position in status line
set ruler

" Ask to save changes when required for command issued
set confirm

" Enable use of the mouse for all modes
set mouse=a

" Two lines for command window
set cmdheight=2

" Turn on auto line wrapping
set lbr

" Enable backspace to go up one indentation level
set backspace=indent,eol,start

" Flag bad whitespace 
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Java
au BufNewFile,BufRead *.java
    \ set foldmethod=syntax |
    \ set tabstop=4         |
    \ set softtabstop=4     |
    \ set shiftwidth=4      |
    \ set textwidth=79      |
    \ set expandtab         |
    \ set autoindent        |
    \ set fileformat=unix

" Python PEP8
au BufNewFile,BufRead *.py
    \ set tabstop=4         |
    \ set softtabstop=4     |
    \ set shiftwidth=4      |
    \ set textwidth=79      |
    \ set expandtab         |
    \ set autoindent        |
    \ set fileformat=unix

" PHP
au BufNewFile,BufRead *.php
    \ set tabstop=4         |
    \ set softtabstop=4     |
    \ set shiftwidth=4      |
    \ set textwidth=79      |
    \ set expandtab         |
    \ set autoindent        |
    \ set fileformat=unix

" Erlang
autocmd BufRead,BufNewFile *.erl,*.es.*.hrl,*.yaws,*.xrl set expandtab
au BufNewFile,BufRead *.erl,*.es,*.hrl,*.yaws,*.xrl setf erlang

" Example for JS, HTML, CSS
au BufNewFile,BufRead *.js,*.html,*.css
    \ set tabstop=2         |
    \ set softtabstop=2     |
    \ set expandtab         |
    \ set shiftwidth=2

" For PHP code folding
let php_folding = 1        "Set PHP folding of classes and functions.
let php_htmlInStrings = 1  "Syntax highlight HTML code inside PHP strings.
let php_sql_query = 1      "Syntax highlight SQL code inside PHP strings.
let php_noShortTags = 1    "Disable PHP short tags.

" Vdebug options for debugging remotely
let g:vdebug_options= {
\    'port' : 9000,
\    'server' : '',
\    'timeout' : 20,
\    'on_close' : 'detach',
\    'break_on_open' : 1,
\    'ide_key' : '',
\    'path_maps' : {},
\    'debug_window_level' : 0,
\    'debug_file_level' : 0,
\    'debug_file' : '',
\    'watch_window_style' : 'expanded',
\    'marker_default' : '⬦',
\    'marker_closed_tree' : '▸',
\    'marker_open_tree' : '▾'
\}

""""""""""""""""""""""""""""""""""""""""
" PLUGINS
""""""""""""""""""""""""""""""""""""""""
call plug#begin()
    Plug 'w0rp/ale'                         "Async lint engine
    Plug 'tmhedberg/SimpylFold'             "Python code folding
    Plug 'Vimjas/vim-python-pep8-indent'    "Python indentation
    Plug 'nvie/vim-flake8'                  "Python PEP8 checking
    Plug 'vim-vdebug/vdebug'                "For Xdebug
    Plug 'scrooloose/nerdtree'              "File explorer
    Plug 'ludovicchabant/vim-gutentags'     "Ctags plugin
    Plug 'majutsushi/tagbar'                "Tag explorer
    "Plug 'ctrlpvim/ctrlp.vim'               "Search
    "Plug '/usr/local/opt/fzf'
    "Plug 'junegunn/fzf.vim'                 "Fuzzy search
    "Plug 'wincent/command-t'                "Fuzzy file search
    "Plug 'digitaltoad/vim-pug'              "pug syntax engine for learn-node
    "Plug 'ajh17/Spacegray.vim'              "Colorscheme
    "Plug 'jnurmine/Zenburn'                 "Colorscheme
    "Plug 'tpope/vim-fugitive'               "git wrapper
    "Plug 'digitaltoad/vim-pug'              "pug syntax engine for learn-node
    "Plug 'vim-erlang/vim-erlang-tags'       "Erlang tags
    "Plug 'scrooloose/nerdtree'              "File explorer
    "Plug 'vim-syntastic/syntastic'          "Check syntax on save
    "Plug 'davidhalter/jedi-vim'             "Autocompletion
    "Plug 'Valloric/YouCompleteMe', { 'do': './install.py'}
    "Plug 'Valloric/YouCompleteMe', { 'do': './install.py', 'on':'YouCompleteMe' }
    "Plug 'l04m33/vlime', {'rtp': 'vim/'}    "Common LISP development environment
    "Plug 'tpope/vim-fireplace'              "For Clojure REPL
    "Plug 'junegunn/vader.vim'               "Vimscript test framework
    "Plug 'Kuniwak/vint'                     "Lint for vimscript
call plug#end()

" Set color scheme
colorscheme desert
"colorscheme spacegray
"colorscheme zenburn

" setup lint engines for ale
let g:ale_fixers = {
\   'php': ['phpcs', 'php -l'],
\   'python': ['flake8', 'pylint'],
\}
