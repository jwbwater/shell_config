"""
Create symbolic links to development environment config files.
"""
import os
import subprocess
import sys

from datetime import datetime
from pathlib import Path

# Where is our home directory?
home = os.environ['HOME']

# What directory are we running in?
dir = Path(sys.argv[0]).resolve().parent

# Create a directory to hold existing config files
backup = Path(dir).joinpath('config-' + datetime.now().isoformat())
backup.mkdir()

# Create {backup}/.config/nvim/autoload
autoload = \
    Path(backup).joinpath('.config').joinpath('nvim').joinpath('autoload')
autoload.mkdir(parents=True, exist_ok=True)

# Create {home}/.config/nvim/autoload if it doesn't exist
autoload = Path(home).joinpath('.config').joinpath('nvim').joinpath('autoload')
autoload.mkdir(parents=True, exist_ok=True)

# Move existing config files out of the way
# and make symbolic links for:
files = [".bashrc",
         ".config/nvim/init.vim",
         ".config/nvim/autoload/plug.vim",
         ".gitconfig",
         ".ssh/rc",
         ".tmux.conf",
         ".zshrc"]

for file in files:
    dest = Path(home).joinpath(file)
    if dest.exists() and not dest.is_symlink():
        subprocess.run(["mv", str(dest), str(backup.joinpath(file))])
    src = Path(dir).joinpath(file)
    if src.exists() and not dest.exists():
        subprocess.run(["ln", "-s", str(src), str(dest)])
