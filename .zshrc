# setup custom aliases
alias h='history'
alias ll='ls -al'
alias acrobat='/Applications/Adobe\ Reader.app/Contents/MacOS/AdobeReader'

# customize command prompt
autoload -U colors && colors
PROMPT="%{$fg[green]%}%T$%{$reset_color%} "

# enable history auto-completion
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward

# setup shared history between terminals and 
# prevent duplicates from being added to history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.history
#setopt share_history
setopt inc_append_history
setopt HIST_IGNORE_ALL_DUPS

# set EDITOR variable so zsh defaults to vi keybindings
EDITOR=nvim

### Added by the Heroku Toolbelt
#export PATH="/usr/local/heroku/bin:$PATH"

# for Docker
#export DOCKER_HOST=tcp://192.168.59.103:2376
#export DOCKER_CERT_PATH=/Users/jwbwater/.boot2docker/certs/boot2docker-vm
#export DOCKER_TLS_VERIFY=1
