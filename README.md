# My dotfiles
Making it easy to duplicate my dev environment on remote servers.
* SSH
* Bash
* Git
* TMUX
* Neovim

## Installation
* install xauth and xclip
* clone repo and type make

## Update existing TMUX session with new configuration
:source-file ~/.tmux.conf

## Cut and Paste in TMUX
xauth is required for X forwarding, which forwards the X clipboard to the
remote machine.

xclip is used to copy text on the remote machine to the X clipboard.

## SSH forwarding in TMUX
Each login to the remote machine generates a new value of $SSH\_AUTH\_SOCK.
This breaks SSH forwarding for existing TMUX sessions. The script in .ssh/rc
makes a symbolic link from ~/.ssh/ssh\_auth\_sock to the value of
$SSH\_AUTH\_SOCK so that TMUX can always use ~/.ssh/ssh\_auth\_sock.

Not entirely sure this is still working after getting Cut and Paste to work
between remote TMUX sessions and my local machine and since Cut and Paste is
more important I have not spent much time testing it. Worst case create a new
TMUX window and use git there.
